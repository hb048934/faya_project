#coding:utf-8


####事件系统
##################################################

#trp_event_temp_details_keeper  
#  这里储存的信息是用于表达以及Bonus中用的，用途多样   下面指的是slot

    # str_event_monarch_friendship_lord_bonus使用      
ETD_give_center = 0
ETD_temp_lord = 1
    # str_event_up_lord_reinforcement  使用
ETD_reinforcement_lord = 2
ETD_reinforcement_bonus_center = 3

#Demand_special_events
DSE_kingdom_has_valid_castle = 0 #国家有有效的城堡（用于加官进爵
DSE_kingdom_aolian_conflict  = 1 #奥利安、帝鹰冲突
DSE_kingdom_dilian_conflict  = 2 #蒂丽嘉、帝鹰冲突

DSE_monarch_kingdom_bonus    = 3 ##帝鹰友善领主给你管辖区
DSE_up_lord_reinforcement    = 4 ##中心采邑的领主给你一定的援助（每周一次）

DSE_player_has_valid_core_center = 20  #玩家有有效核心采邑

DSE_kingdom_is_north_basis = 21 #是北境国家

DSE_123 = 3
#特殊操作替代符

E_operation_do_script = "str_E_operation_do_script"
E_save_operation_val  = "str_E_save_operation_val"
E_save_val_in_trp_list = "str_E_save_val_in_trp_list"
E_get_val_to_E_val_from_list = "str_E_get_val_to_E_val_from_list"
E_operation_to_player    = "str_E_operation_to_player"


##############

#trp_event_keeper_i_slot_type
E_temp_save_val_0 = -11
E_temp_save_val_1 = -12
E_temp_save_val_2 = -13
E_temp_save_val_3 = -14

E_temp_get_val_0 = -21#这部分不会被转换
E_temp_get_val_1 = -22
E_temp_get_val_2 = -23
E_temp_get_val_3 = -24

E_tag_troop_type = -30
E_tag_party_type = -31
E_tag_faction_type = -32

###
E_object_faction = -125
E_object = -126
E_object_faction_leader = -128
E_object_core_center = -130 #对象的核心采邑



#E_tag_object = -126  ##转化成object，对for_world无效
#E_tag_player_core_center = -127 ##转化为玩家核心采邑
#E_tag_player_kingdom_lord = -128  ##转化为玩家的阵营领袖
#E_tag_player_kingdom = -129  ##转化为玩家的国家

E_no_val = -1011

#特殊操作
E_operation_get_val = "str_e_operation_get_val"
E_operation_on_quest = "str_e_operation_on_quest"
E_operation_save_string_in = "str_e_operation_save_string_in"
E_operation_check_special_notes = "str_e_operation_check_special_notes"
E_operation_set_troop_slot = "str_e_operation_set_troop_slot"
E_operation_add_notes = "str_e_operation_add_notes"
E_operation_jump_to_menu = "str_e_operation_jump_to_menu"

#事件需要的条件
E_demand_faction_slot_eq = "str_e_demand_faction_slot_eq"
E_demand_day_ge = "str_e_demand_day_ge"
E_demand_no_weekly_trigger = "str_e_demand_no_weekly_trigger"
E_demand_despotism_gover = "str_e_demand_despotism_gover"
E_demand_quest_operation = "str_e_demand_quest_operation"
E_demand_npc_kingdom = "str_e_demand_npc_kingdom"
E_demand_specific_object = "str_e_demand_specific_object"
E_demand_quest_slot_ge = "str_e_demand_quest_slot_ge"
E_demand_troop_slot_eq = "str_e_demand_troop_slot_eq"
E_demand_troop_slot_ge = "str_e_demand_troop_slot_ge"
E_demand_party_slot_eq = "str_e_demand_party_slot_eq"
E_demand_history_mode = "str_e_demand_history_mode"
E_demand_faction_either_has_ks = "str_e_demand_faction_either_has_ks"

E_demand_faction_capital_area = "str_e_demand_faction_capital_area"

E_demand_special_events = "str_e_demand_special_events"
E_demand_global_val = "str_e_demand_global_val"
EDG_val_for_fate_control = 1


#产生的效果
#特殊情况
E_bonus_change_kingdom_state_to_state = "str_e_bonus_change_kingdom_state_to_state" 
E_bonus_change_kingdom_type_to_type = "str_e_bonus_change_kingdom_type_to_type"
E_bonus_kingdom_get_amendment_or_undock = "str_e_bonus_kingdom_get_amendment_or_undock"
E_bonus_spawn_kingdom = "str_e_bonus_spawn_kingdom"
E_bonus_start_war_between_kingdom = "str_e_bonus_start_war_between_kingdom"

E_bonus_trigger_event = "str_e_bonus_trigger_event"

#party获得
E_bonus_center_change_basic_rents = "str_e_bonus_center_change_basic_rents"

#XXX获得
E_bonus_troop_get_right_to_rule = "str_e_bonus_troop_get_right_to_rule"
E_bonus_troop_get_gold = "str_e_bonus_troop_get_gold"
E_bonus_troop_get_administration = "str_e_bonus_troop_get_administration"
E_bonus_troop_get_exploit = "str_e_bonus_troop_get_exploit"
E_bonus_troop_renown = "str_e_bonus_troop_renown"

#faction获得
E_bonus_faction_finish_tec = "str_e_bonus_faction_finish_tec"

#玩家专属
E_bonus_player_max_renown = "str_e_bonus_player_max_renown"
E_bonus_player_honor = "str_e_bonus_player_honor"
E_bonus_special = "str_e_bonus_special"



#事件冷却时间的特殊字符
Event_only_once = "str_event_only_once"##只触发一次
Event_no_duration = -1 ##事件的触发没有冷却

#范围
Event_tag_begin = "str_event_world_war_is_coming"
Event_tag_end = "str_event_tag_end"

Event_confirm_tag_begin = "str_event_world_war_is_coming_confirm"
Event_confirm_tag_end = "str_event_confirm_end"

Event_operation_tag_begin = "str_e_operation_get_val"
Event_operation_tag_end   = "str_e_operation_end"


Event_max_num = 256

E_type_event = 1
E_type_confirm = 2
E_max_confirm_num = 12 ##设定每个事件最多有6个选项（太少了，来12个）


######
Max_num_for_player_notes = 100 ##简单事件储存的数量
Max_slot_each_player_notes = 10 ##每个最多10个储存槽
Main_notes_for_kingdom_state      = -10  #国家状态改变时的提示
Main_notes_for_player             = -11  #给玩家的提示信息，比如国家事件发生后的事情
Main_notes_for_simple_intro       = -12  #简单触发，如提示，或战场提示
Main_notes_for_war_policy         = -13  #战争政治事件
####
#trp_E_gather_temp
#E_gather_num_save_slot = 100
Max_slot_for_war_save = 20
Max_num_for_player_war_notes = 100

#trp_player_hints_list
PHN_hints_title_string_begin = "str_phn_simple_notes_title_1"




#                                               改革和路线
####################################################################################################3
Tec_finished = -120  ##完成任务  
Tec_no = -121 ##没有这个科技

TEC_class = 1
TEC_premise = 2
TEC_weight = 3 ##权重
TEC_des = 4 ##描述
TEC_pathway = 5 #路线
TEC_only_for_pathway = 1  #这个是配合上面的使用的

Tec_begin = "str_tec_commercial_bill"
Tec_end = "str_tec_end"
#trp_tec_obj_line_keeper
#0:线条obj     0+Tec_line_max_num:该线条obj的init_x

#trp_tec_obj_keeper  储存的东西，
Tec_max_num = 300    #用于obj
#0：tec_obj    0+Tec_max_num：init_x
Tec_line_max_num = 500  ##这个只是储存画了多少条线到 500 这个槽

# trp_tec_temp_keeper_for_obj_num
TK_tec_line_max_num = 0 #储存画了多少条线到这个槽里
TK_tec_cur_chosen_tec = 1##储存当前查看的tec
TK_tec_cur_chosen_tec_obj = 2 #储存当前改革的那个黑框
TK_tec_line_max_obj = 3 ##储存最大line obj 数量
TK_tec_pren_bonus_x = 4 #这里储存的是滑动条记录的
TK_tec_cur_target_tec_progress_obj = 5 #推进进度条
TK_tec_save_for_pren = 6 #国家的科技数量



Tec_idea_dec_begin = "str_tec_monarch_idea_1_des"
Tec_idea_begin = "str_tec_monarch_idea_1"
Tec_idea_end = Tec_end


###############

KSR_infinite_time = 999999

#KSR_cur_kingdom = -100







#######世界事件		
#trp_world_event_progress

WEP_snow_disaster = 0   ##雪灾
WEP_snow_disaster_fate = 1 ##北境的命运
#1、 与黑山决裂   后会将这个槽设置为3，开始刷出反抗军与清缴队
#2、 黑山与冬湖和好
#3、 黑山与格雷德彻底开战
##   
WEP_snow_fate_normal = 0    #初期，没东西
WEP_snow_fate_local_deterioration = 1   #局部恶化
WEP_snow_fate_contact_with_dark_river = 2#与黑山接触
WEP_snow_fate_refuse_dark_river = 3 #拒绝黑山

WEP_snow_end_cond = 100 #暂时终结

WEP_clear_kingdom_state = 100
#1、清除了国家修正
####

#

########################
####玩家剧情保存
special_troop_set_begin = "trp_fate_card_keeper"
special_troop_set_end = "trp_mer_list"

normal_troop_end = "trp_antique_merchant_end"

##主要是弥莎的剧情
#script_cf_player_trivial_eq,TEVENT_sushan_gift,0
#script_player_trivial_set,TEVENT_sushan_gift,1

###trp_player_trivial         trp_fate_card_keeper#用于储存用过的命牌
TEVENT_sushan_gift = 0  #苏珊的药物  1：已经吃过一次
TEVENT_missa_fate_card = 1 #弥莎命牌， 1为开启

TEVENT_missa_event_1 = 10  ##触发雨
TEVENT_missa_event_2 = 11  ##触发喜欢雨天
TEVENT_missa_event_3 = 12  ##杰尔卡国王
TEVENT_missa_event_4 = 13  ##弥莎占卜规则
TEVENT_missa_event_5 = 14  ##海姆
TEVENT_missa_event_6 = 15  ##日记本
TEVENT_missa_event_7 = 16  ##马车
TEVENT_missa_event_8 = 17  ##遇到苍都
TEVENT_missa_event_9 = 18  ##在酒馆拼酒
TEVENT_missa_event_10= 19  ##弥莎醉了

TEVENT_missa_event_11= 30  ##认为布兰的服饰特别
TEVENT_missa_event_12= 31  ##赞赏月华

TEVENT_missa_event_13= 32  ##苍都精灵讯息



#国家状态里的信息
KS_i_title_slot = 0
KS_i_mesh_slot = 1
KS_i_intro_slot = 2
KS_i_duration_time = 10

#
OUT_ui_val_normal = 1
OUT_ui_val_add_or_sub = 2 #正常的+ -  +为绿，-为红
OUT_ui_val_add_or_sub_ratio = 3 #正常的+ - %,+为绿，-为红
OUT_ui_val_add_or_sub_reverse = 4 #-为绿，+为红
OUT_ui_val_add_or_sub_ratio_reverse = 5 #%,-为绿，+为红

#输出字符里的特殊标志
OUT_ui_normal_add = 1
OUT_ui_normal_sub = 2
OUT_ui_ratio_add  = 3
OUT_ui_ratio_sub  = 4
OUT_ui_final_result = 10

#特殊
#CS_script_center_building_power = 0

##讯息trp_notes_keeper
Faya_note_begin = "str_note_of_faya_begin"
Faya_note_end = "str_note_of_faya_end"


##
#trp_player_record
PR_assault_caravan = 0

PR_susan_give_zanghua = 1  #1、下次见面给葬花   2、已经给了，完毕


PR_player_get_gold_statue = 10 # 1、取得过

PR_player_keep_book_item_2_time     = 20 ##玩家持有卷轴时间
PR_player_keep_book_item_2_progress = 21 ##玩家的卷轴事件推进

PR_player_butcher         = 49 #1、玩家屠城过
PR_kingdom_1_finish_idea_2 = 50 #1、开始   2-6：随机增加骑兵、弩兵或步兵  7、结束  8、不再触发
###########################################


###记录战争信息 trp_War_faction_record   ##$War_record_slot
#WR_winner_faction = 0
WR_attacker = 0  ##储存阵营
WR_attacker_before = 1
WR_attacker_rest = 2
WR_defender = 3
WR_defender_before = 4
WR_defender_rest = 5
WR_score = 6
WR_site = 7
WR_hours = 8
WR_winner = 9
WR_attacker_leader = 10
WR_defender_leader = 11
WR_defender_center = 12

WR_notes_end = 13

WR_cond_num = WR_notes_end

Max_WR_notes_num = 150

WR_special_bonus_limit = 4000


#####













########一些基础设置
max_caravan_num = 200



############gal的各种常量
#################
Gal_begin = "trp_gal_every_details_1"
#Gal_end = "trp_gal_every_details_end"  Gal_end指的是结束gal对话，不是一个troop

Gal_list_begin = "trp_gal_list_1"
Gal_list_end = "trp_gal_list_end"

Gal_details_begin = "trp_gal_details_list_1"
            #############################
##gal系统   #    trp_gal_things_keeper   #
            #############################
Gal_target_string = 0
Gal_cur_cond = 1
Gal_num_option = 2

Gal_scene_mesh = 5

Gal_left_troop  = 10
Gal_mid_troop   = 11
Gal_right_troop = 12

Gal_troop_end = 13
Gal_expression_1 = 13
Gal_troop_expression_end = 16

Gal_option_1 = 20  ##

Gal_option_end = 30
Gal_option_1_obj = 30##
Gal_option_obj_end = 40#
#################
#####################


Result_begin = "str_t_begin"
Result_end = "str_t_end"


To_menu = "str_t_jump_to_menu"
To_set_troop_slot = "str_t_set_troop_slot"
To_enable_fate_card_system = "str_t_enable_fate_card_system"

To_remove_troop_item = "str_t_remove_troop_item"
To_give_troop_item = "str_t_give_troop_item"
To_clear_troop_gal_seat = "str_t_clear_troop_gal_seat"##清除站位
To_change_troop_gal_expression = "str_t_change_troop_gal_expression"##改变表情

To_get_note  = "str_t_get_note"##获得讯息
To_set_mingpai_system = "str_t_set_mingpai_system"##开启命牌系统
#####


###########
Gal_start = "str_gal_start_trigger"
Gal_end = "str_gal_close_window"

Demand_first_met = "str_demand_first_met"
Demand_troop_slot_eq = "str_demand_troop_slot_eq"  ##某单位某slot的值为trp,slot,2

##设置一些效果
Demand_set_gal_scene = "str_demand_set_gal_scene"##设置背景
Demand_set_troop_seat = "str_demand_set_troop_seat"##设置站位，troop_no ,Gal_mid_troop/Gal_left_troop/Gal_right_troop


Demand_talk_in_tavern = "str_demand_talk_in_tavern"##酒馆交谈
Demand_time_since_last_talk = "str_demand_time_since_last_talk"
Demand_finish_fate_card = "str_demand_finish_fate_card"
#player_event_keeper
PE_player_friend = 11 #玩家在最开始结识的朋友
PE_watching_npc = 12  #王放在玩家旁边监视的NPC 

PE_missa_gem_type = 20 ##沙盒模式中弥莎的宝石状态，最初-1表示没事情，1表示开始记录灵魂强度
PE_player_soul_power = 21##玩家灵魂强度

PE_player_saint_temple = 22##玩家在圣塔罗教堂获得伊耶塔讯息，-1为没有获得  1为获得
PE_player_has_close_gem = 23##玩家的两个耀石放的太近  1：准备爆炸

PE_laen_event = 30###莱昂  #-2、终结   -1、触发    0、开始和玩家的初次对话  
                           #1、玩家碰到的队伍是非法骑士，随机出了莱昂碰瓷
						   #如果到了1，那么每周+1，直到5停止
						   #8：到了战场中
						   #10:莱昂归队，准备去找宝藏
PE_laen_event_center = 31 ###莱昂任务县城
PE_npc2_leave = 32 ###米兰森离开事件   1：米兰森准备离开   2：米兰森准备加入帝鹰  3：米兰森加入帝鹰  -2：米兰森死亡  4：在战场中被击败，过渡用
PE_npc3_leave = 33 ###玛丽事件  1：玛丽送紫耀石  2：玛丽准备离开  3：玛丽失踪

PE_missa_progress = 40  #弥莎事件  1、弥莎邀请玩家到公会     2、已经结识弥莎
                        #          10、奥菲娅与弥莎见完面
PE_missa_temp_slot = 41 ##临时储存的东西   -1:指向弥莎在胧月公会与玩家见面    -2：无特殊指定    0：玩家没穿衣服初次见面   1:玩家初次见面
                        ##2：玩家苏醒 
						##3:玩家完成交易
                        ##10、奥菲娅任务  
						
						##弥莎      11：触发喜欢雨天  12：杰尔卡国王
						##      13：占卜规则  14：海姆   15：日记本  16：马车
						##      17：遇到苍都  18：在酒馆拼酒   19：弥莎醉了  20：触发雨

PE_aofeiya_story = 60 ###奥菲娅事件
                        # 1：开启剧情  11：离开了初期剧情  12：完成招募任务
PE_party_id_save_1 = 61 #储存一个party id 
PE_party_id_save_2 = 62                  



PE_player_hint = 80	##玩家的提示任务					
PE_player_cur_jianlan_trigger = 81 ##和剑兰有关的trigger  1：玩家打败队伍，剑兰准备出现

PE_player_kingdom_has_cabinet = 150 #1：玩家阵营形成了内阁
PE_player_change_cloth_for_wife = 151 #1：玩家给老婆换衣服
PE_player_get_armor_from_kingdom = 152 #1：已经拿了国家的装备

PE_player_has_rebel = 200  ##1：玩家背叛过阵营

PE_player_modi_recruit_lord = 300    ##玩家修改直接招募领主

PE_game_max_slot_for_tec = 350 ##用于储存tec的最大槽数





outlaw_faction_pt_begin = "pt_troupe_outlaw"
outlaw_faction_pt_end   = "pt_outlaw_faction_end"
 #####################野怪
 #trp_world_bandits_keeper     
 #
 #
 #
 #
 #

#WBK_green_bandits_score = 0  ##绿林
#WBK_green_bandits_spawn_chance = 1 刷出的概率
#
#绿林是针对帝鹰的野怪，会产生一个巢穴，巢穴周围产生带残党的部队
#中部地区的民众支持低于80的采邑会
#初期具有30.0%的强度
#
#
############# Tag  必须小于 slot_cb_bonus_center_pop_grow 的 50
I_tag_inefficiency_management = 1
I_tag_trait_piety_of_elyse = 2

I_tag_faction_blank = 20 

I_tag_skill_engineer = 40  
##############################

#slot_center_methopolis 中心采邑（在constants里预设了有20个建筑槽，其中包含主城槽）
#slot_center_building_slot_1
#
CB_keepers_begin = "trp_cb_methopolis_center_normal"
CB_keepers_end   = "trp_cb_end"

CB_i_building_slot_begin = 0
CB_i_building_slot_end = 256   #这个要囊括所有用到的slot


slot_cb_demand_center_methopolis_class = 1  #需求中心等级
slot_cb_demand_gold = 2 #花费金钱（必须以1000的倍数
slot_cb_demand_building_points  = 3 #基础需要的建设点数

slot_cb_demand_center_type = 6 #需求采邑种类


slot_cb_demmands_begin = slot_cb_demand_center_methopolis_class
slot_cb_demmands_end = 30


slot_cb_update_building      = 30    #升级建筑（上级）
slot_cb_mesh              =   31
slot_cb_intro_note      = 32
slot_cb_class_level         = 33  #是几级的
slot_cb_type            = 34    #种类    
Sct_center = 1#市中心
Sct_military = 2#军事序列
Sct_improvement = 3#发展
Sct_order       = 4#秩序
Sct_landmark    = 5#地标
Sct_blank       = 6#空地

slot_cb_basic_building  = 35
#基础是哪个，这里如果是1，表示可以直接建设，
#如果是大于1，表示是某个建筑升级上来的
slot_cb_max_building_slot     = 36  #主城序列带来的最大建筑槽数（仅主城序列可以拥有）

#采邑的稳定与增幅
slot_cb_bonus_center_pop_grow   = 50 #人口增长速度，这里的1代表0.01%
slot_cb_bonus_center_awake      = 51 #本地觉醒+
slot_cb_bonus_center_stability  = 52 #稳定性+
slot_cb_bonus_center_peace      = 53 #治安恢复+
slot_cb_bonus_building_efficiency = 54 #建筑效率+%

slot_cb_bonus_center_human_support  = 55 #民众支持+
slot_cb_bonus_center_noble_support  = 56 #贵族支持+
slot_cb_bonus_center_policy_ratio   = 57 #行政效率+%

#税收与贸易
slot_cb_bonus_center_people_rents   = 58 #民众税收+%
slot_cb_bonus_center_noble_rents    = 59 #贵族税收+%
slot_cb_bonus_center_rents_ratio    = 60 #采邑税收+%
slot_cb_bonus_center_basic_rents    = 61 #采邑基础税收


slot_cb_bonus_center_manufacture_power      = 62#贸易额+% 
slot_cb_bonus_center_trade_competitiveness  = 63#贸易竞争力+%
slot_cb_bonus_center_trade_effeciency       = 64#贸易效率+%
#补给
slot_cb_bonus_center_supply_max_limit   = 65 #采邑补给上限+
slot_cb_bonus_center_supply_local_cost  = 66 #本地补给消耗（这里+为负面效果）
slot_cb_bonus_center_supply_output_effecicency = 67 #补给产出+%

#训练与募兵
slot_cb_bonus_center_training_speed = 68 #训练速度+%
slot_cb_bonus_center_train_degress  = 69 #训练度
slot_cb_bonus_center_h_r_bonus      = 70 #人力增长速度+%
slot_cb_bonus_center_h_r_max_limit  = 71 #人力最大值+%
slot_cb_bonus_human_resource_ratio  = 72 #人力利用率+%

#要塞
slot_cb_bonus_castle_fortress_class = 73 #要塞等级



slot_cb_bonus_center_prisoner_escape_chance = 80 #犯人逃跑概率-%

slot_cb_center_enable_recruit = 81  #允许征募民众
slot_cb_center_enable_militia = 82  #允许征募民兵


#slot_cb_enable_army_recruit_with_le_level = 70  #专制国家，该等级以下的士兵可以训练

#在这里，增益的结果范围
slot_cb_bonus_begin = slot_cb_bonus_center_pop_grow
slot_cb_bonus_end = 100

#下面开始是该建筑解锁的可招募士兵
slot_cb_can_train_army_slot_num = 100
slot_cb_can_train_army_begin = 101


